package br.com.jorgerabellodev.agenda.database;

import static br.com.jorgerabellodev.agenda.model.TipoTelefone.FIXO;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;
import br.com.jorgerabellodev.agenda.model.TipoTelefone;

class AgendaMigrations {

  private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
      database.execSQL("ALTER TABLE aluno ADD COLUMN sobrenome TEXT");
    }
  };

  private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
      database.execSQL(
          "CREATE TABLE IF NOT EXISTS `aluno_novo` "
              + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
              + "`nome` TEXT, "
              + "`telefone` TEXT, "
              + "`email` TEXT)"
              + "");

      database.execSQL("INSERT INTO aluno_novo(id, nome, telefone, email)"
          + "SELECT id, nome, telefone, email FROM Aluno");

      database.execSQL("DROP TABLE Aluno");

      database.execSQL("ALTER TABLE aluno_novo RENAME TO Aluno");
    }
  };

  private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
      database.execSQL("ALTER TABLE aluno ADD COLUMN dataDeCadastro INTEGER");
    }
  };

  private static final Migration MIGRATION_4_5 = new Migration(4, 5) {
    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
      database.execSQL("CREATE TABLE IF NOT EXISTS `aluno_novo` "
          + "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
          + "`nome` TEXT, "
          + "`telefoneFixo` TEXT, "
          + "`telefoneCelular` TEXT, "
          + "`email` TEXT, "
          + "`dataDeCadastro` INTEGER)"
      );

      database.execSQL(
          "INSERT INTO aluno_novo(id, nome, telefoneFixo, email, dataDeCadastro)"
              + "SELECT id, nome, telefone, dataDeCadastro, email FROM Aluno");

      database.execSQL("DROP TABLE Aluno");

      database.execSQL("ALTER TABLE aluno_novo RENAME TO Aluno");

    }
  };

  private static final Migration MIGRATION_5_6 = new Migration(5, 6) {
    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
      database.execSQL(
          "CREATE TABLE IF NOT EXISTS `aluno_novo` ("
              + "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
              + "`nome` TEXT, "
              + "`email` TEXT, "
              + "`dataDeCadastro` INTEGER)");

      database.execSQL(
          "INSERT INTO aluno_novo(id, nome, email, dataDeCadastro)"
              + "SELECT id, nome, email, dataDeCadastro FROM Aluno");

      database.execSQL(
          "CREATE TABLE IF NOT EXISTS `Telefone` ("
              + "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
              + "`numero` TEXT, "
              + "`tipo` TEXT, "
              + "`aluno_id` INTEGER NOT NULL)");

      database.execSQL(
          "INSERT INTO Telefone(numero, aluno_id)"
              + "SELECT telefoneFixo, id FROM Aluno");

      database.execSQL("UPDATE Telefone SET tipo = ?", new TipoTelefone[]{FIXO});

      database.execSQL("DROP TABLE Aluno");

      database.execSQL("ALTER TABLE aluno_novo RENAME TO Aluno");


    }
  };

  private static final Migration MIGRATION_6_7 = new Migration(6, 7) {
    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {

      database.execSQL("DROP TABLE Aluno");
      database.execSQL("DROP TABLE Telefone");

      database.execSQL(
          "CREATE TABLE IF NOT EXISTS `Telefone` ("
              + "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
              + "`numero` TEXT, "
              + "`tipo` TEXT, "
              + "`aluno_id` INTEGER NOT NULL)");

      database.execSQL(
          "CREATE TABLE IF NOT EXISTS `Aluno` ("
              + "`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
              + "`nome` TEXT, "
              + "`email` TEXT, "
              + "`dataDeCadastro` INTEGER)");

    }
  };
  static final Migration[] TODAS_MIGRATIONS = {
      MIGRATION_1_2,
      MIGRATION_2_3,
      MIGRATION_3_4,
      MIGRATION_4_5,
      MIGRATION_5_6,
      MIGRATION_6_7
  };

}
