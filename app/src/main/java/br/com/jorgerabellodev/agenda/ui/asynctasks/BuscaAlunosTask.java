package br.com.jorgerabellodev.agenda.ui.asynctasks;

import android.os.AsyncTask;
import br.com.jorgerabellodev.agenda.database.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.ui.adapter.ListaAlunosAdapter;
import java.util.List;

public class BuscaAlunosTask extends AsyncTask<Void, Void, List<Aluno>> {

  private final ListaAlunosAdapter adapter;
  private final AlunoDAO dao;

  public BuscaAlunosTask(ListaAlunosAdapter adapter, AlunoDAO dao) {
    this.adapter = adapter;
    this.dao = dao;
  }


  @Override
  protected List<Aluno> doInBackground(Void[] objects) {
    return dao.todos();
  }

  @Override
  protected void onPostExecute(List<Aluno> todosOsAlunos) {
    super.onPostExecute(todosOsAlunos);
    adapter.atualza(todosOsAlunos);
  }
}
