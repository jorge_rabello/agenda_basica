package br.com.jorgerabellodev.agenda.database.converters;

import android.arch.persistence.room.TypeConverter;
import br.com.jorgerabellodev.agenda.model.TipoTelefone;

public class ConversorTipoTelefone {

  @TypeConverter
  public String paraString(TipoTelefone valor) {
    return valor.name();
  }

  @TypeConverter
  public TipoTelefone paraTipoTelefone(String valor) {
    if (valor != null) {
      return TipoTelefone.valueOf(valor);
    }
    return TipoTelefone.FIXO;
  }

}
