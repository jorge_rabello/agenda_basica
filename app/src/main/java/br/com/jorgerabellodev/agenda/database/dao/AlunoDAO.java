package br.com.jorgerabellodev.agenda.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import br.com.jorgerabellodev.agenda.model.Aluno;
import java.util.List;

@Dao
public interface AlunoDAO {

  @Insert
  Long salva(Aluno aluno);

  @Query("SELECT * FROM aluno")
  List<Aluno> todos();

  @Delete
  void remove(Aluno aluno);

  @Update
  void edita(Aluno aluno);
}
