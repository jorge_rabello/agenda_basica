package br.com.jorgerabellodev.agenda.database;

import static br.com.jorgerabellodev.agenda.database.AgendaMigrations.TODAS_MIGRATIONS;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import br.com.jorgerabellodev.agenda.database.converters.ConversorCalendar;
import br.com.jorgerabellodev.agenda.database.converters.ConversorTipoTelefone;
import br.com.jorgerabellodev.agenda.database.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.model.Telefone;

@Database(entities = {Aluno.class, Telefone.class}, version = 7, exportSchema = false)
@TypeConverters({ConversorCalendar.class, ConversorTipoTelefone.class})
public abstract class AgendaDatabase extends RoomDatabase {

  private static final String DB_NAME = "agenda.db";

  public abstract AlunoDAO getRoomAlunoDAO();

  public abstract TelefoneDao getTelefoneDao();

  public static AgendaDatabase getInstance(Context context) {
    return Room
        .databaseBuilder(context, AgendaDatabase.class, DB_NAME)
        .addMigrations(TODAS_MIGRATIONS)
        .build();
  }

}
