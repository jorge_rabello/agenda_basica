package br.com.jorgerabellodev.agenda.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.jorgerabellodev.agenda.R;
import br.com.jorgerabellodev.agenda.database.AgendaDatabase;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.ui.asynctasks.BuscaPrimeiroTelefoneDoAlunoTask;
import java.util.ArrayList;
import java.util.List;

public class ListaAlunosAdapter extends BaseAdapter {

  private final Context context;
  private final List<Aluno> alunos = new ArrayList<>();
  private final TelefoneDao dao;

  public ListaAlunosAdapter(Context context) {
    this.context = context;
    dao = AgendaDatabase.getInstance(context).getTelefoneDao();
  }

  @Override
  public int getCount() {
    return alunos.size();
  }

  @Override
  public Aluno getItem(int position) {
    return alunos.get(position);
  }

  @Override
  public long getItemId(int position) {
    return alunos.get(position).getId();
  }

  @Override
  public View getView(int position, View convertView, ViewGroup viewGroup) {
    View viewCriada = criaView(viewGroup);
    Aluno aluno = alunos.get(position);
    vincula(viewCriada, aluno);
    return viewCriada;
  }

  private void vincula(View view, Aluno aluno) {
    TextView nome = view.findViewById(R.id.item_aluno_nome);
    nome.setText(aluno.getNomeCompleto());

    TextView telefone = view.findViewById(R.id.item_aluno_telefone);

    new BuscaPrimeiroTelefoneDoAlunoTask(dao, aluno.getId(),
        telefoneEncontrado -> telefone.setText(telefoneEncontrado.getNumero())
    ).execute();

  }

  private View criaView(ViewGroup view) {
    return LayoutInflater.from(context).inflate(R.layout.item_aluno, view, false);
  }

  public void atualza(List<Aluno> alunos) {
    this.alunos.clear();
    this.alunos.addAll(alunos);
    notifyDataSetChanged();
  }

  public void remove(Aluno aluno) {
    alunos.remove(aluno);
    notifyDataSetChanged();
  }
}
