package br.com.jorgerabellodev.agenda.model;

public enum TipoTelefone {
  FIXO, CELULAR
}
