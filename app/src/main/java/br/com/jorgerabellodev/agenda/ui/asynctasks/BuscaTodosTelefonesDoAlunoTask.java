package br.com.jorgerabellodev.agenda.ui.asynctasks;

import android.os.AsyncTask;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.model.Telefone;
import java.util.List;

public class BuscaTodosTelefonesDoAlunoTask extends AsyncTask<Void, Void, List<Telefone>> {

  private final TelefoneDao telefoneDao;
  private final Aluno aluno;
  private final TelefonesDoAlunoEncontradoListener listener;

  public BuscaTodosTelefonesDoAlunoTask(TelefoneDao telefoneDao, Aluno aluno,
      TelefonesDoAlunoEncontradoListener listener) {
    this.telefoneDao = telefoneDao;
    this.aluno = aluno;
    this.listener = listener;
  }

  @Override
  protected List<Telefone> doInBackground(Void... voids) {
    return telefoneDao.buscaTodosTelefonesDoAluno(aluno.getId());
  }

  @Override
  protected void onPostExecute(List<Telefone> telefones) {
    super.onPostExecute(telefones);
    listener.quandoEncontrados(telefones);
  }

  public interface TelefonesDoAlunoEncontradoListener {

    void quandoEncontrados(List<Telefone> telefones);
  }
}
