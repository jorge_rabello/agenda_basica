package br.com.jorgerabellodev.agenda.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import java.io.Serializable;
import java.util.Calendar;

@Entity
public class Aluno implements Serializable {

  @PrimaryKey(autoGenerate = true)
  private int id = 0;
  private String nome;
  private String email;
  private Calendar dataDeCadastro = Calendar.getInstance();


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getNomeCompleto() {
    return this.nome;
  }

  public Calendar getDataDeCadastro() {
    return dataDeCadastro;
  }

  public void setDataDeCadastro(Calendar dataDeCadastro) {
    this.dataDeCadastro = dataDeCadastro;
  }

  public boolean temIdValido() {
    return this.id > 0;
  }

  @NonNull
  @Override
  public String toString() {
    return this.nome;
  }

}
