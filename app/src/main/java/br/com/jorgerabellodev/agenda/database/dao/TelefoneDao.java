package br.com.jorgerabellodev.agenda.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import br.com.jorgerabellodev.agenda.model.Telefone;
import java.util.List;

@Dao
public interface TelefoneDao {

  @Query("SELECT * FROM Telefone WHERE aluno_id = :alunoId LIMIT 1")
  Telefone buscaPrimeiroTelefoneDoAluno(int alunoId);

  @Insert
  void salva(Telefone telefoneFixo, Telefone telefoneCelular);

  @Query("SELECT * FROM Telefone WHERE aluno_id = :alunoId")
  List<Telefone> buscaTodosTelefonesDoAluno(int alunoId);

  @Update
  void atualiza(Telefone... telefones);
}
