package br.com.jorgerabellodev.agenda.ui.asynctasks;

import br.com.jorgerabellodev.agenda.database.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.model.Telefone;
import br.com.jorgerabellodev.agenda.model.TipoTelefone;
import java.util.List;

public class EditaAlunoTask extends BaseAlunoComTelefoneTask {

  private final AlunoDAO alunoDao;
  private final Aluno aluno;
  private final Telefone telefoneFixo;
  private final Telefone telefoneCelular;
  private final TelefoneDao telefoneDao;
  private final List<Telefone> telefonesDoAluno;

  public EditaAlunoTask(AlunoDAO alunoDao,
      Aluno aluno,
      Telefone telefoneFixo,
      Telefone telefoneCelular,
      TelefoneDao telefoneDao,
      List<Telefone> telefonesDoAluno,
      FinalizadaListener listener) {

    super(listener);
    this.alunoDao = alunoDao;
    this.aluno = aluno;
    this.telefoneFixo = telefoneFixo;
    this.telefoneCelular = telefoneCelular;
    this.telefoneDao = telefoneDao;
    this.telefonesDoAluno = telefonesDoAluno;

  }

  @Override
  protected Void doInBackground(Void... voids) {
    alunoDao.edita(aluno);
    vinculaAlunoComTelefone(aluno.getId(), telefoneFixo, telefoneCelular);
    atualizaIdsDosTelefones(telefoneFixo, telefoneCelular);
    telefoneDao.atualiza(telefoneFixo, telefoneCelular);
    return null;
  }


  private void atualizaIdsDosTelefones(Telefone telefoneFixo, Telefone telefoneCelular) {
    for (Telefone telefone : telefonesDoAluno) {
      if (telefone.getTipo() == TipoTelefone.FIXO) {
        telefoneFixo.setId(telefone.getId());
      } else {
        telefoneCelular.setId(telefone.getId());
      }
    }
  }

}
