package br.com.jorgerabellodev.agenda.ui.activity;

import static br.com.jorgerabellodev.agenda.ui.activity.ConstantesActivities.CHAVE_ALUNO;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import br.com.jorgerabellodev.agenda.R;
import br.com.jorgerabellodev.agenda.database.AgendaDatabase;
import br.com.jorgerabellodev.agenda.database.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.model.Telefone;
import br.com.jorgerabellodev.agenda.model.TipoTelefone;
import br.com.jorgerabellodev.agenda.ui.asynctasks.BuscaTodosTelefonesDoAlunoTask;
import br.com.jorgerabellodev.agenda.ui.asynctasks.EditaAlunoTask;
import br.com.jorgerabellodev.agenda.ui.asynctasks.SalvaAlunoTask;
import java.util.List;

public class FormularioAlunoActivity extends AppCompatActivity {

  private EditText campoNome;
  private EditText campoTelefoneFixo;
  private EditText campoTelefoneCelular;
  private EditText campoEmail;
  private Aluno aluno;
  private AlunoDAO alunoDao;
  private TelefoneDao telefoneDao;
  private List<Telefone> telefonesDoAluno;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_formulario_aluno);

    AgendaDatabase database = AgendaDatabase.getInstance(this);
    alunoDao = database.getRoomAlunoDAO();
    telefoneDao = database.getTelefoneDao();

    inicializacaoDosCampos();
    carregaDadosDoAluno();
  }

  private void carregaDadosDoAluno() {
    Intent dados = getIntent();
    if (dados.hasExtra(CHAVE_ALUNO)) {
      setTitle(getResources().getString(R.string.edita_aluno));
      aluno = (Aluno) dados.getSerializableExtra(CHAVE_ALUNO);
      if (aluno != null) {
        preencheCampos();
      }
    } else {
      setTitle(getResources().getString(R.string.novo_aluno));
      aluno = new Aluno();
    }
  }

  private void preencheCampos() {
    campoNome.setText(aluno.getNome());
    campoEmail.setText(aluno.getEmail());
    preencheCamposDeTelefone();
  }

  private void preencheCamposDeTelefone() {
    new BuscaTodosTelefonesDoAlunoTask(telefoneDao, aluno, telefones -> {
      this.telefonesDoAluno = telefones;
      for (Telefone telefone : telefonesDoAluno) {
        if (telefone.getTipo() == TipoTelefone.FIXO) {
          campoTelefoneFixo.setText(telefone.getNumero());
        } else {
          campoTelefoneCelular.setText(telefone.getNumero());
        }
      }
    }).execute();

  }

  private void finalizaFormulario() {
    preencheAluno();

    Telefone telefoneFixo = criaTelefone(campoTelefoneFixo, TipoTelefone.FIXO);
    Telefone telefoneCelular = criaTelefone(campoTelefoneCelular, TipoTelefone.CELULAR);

    if (aluno.temIdValido()) {
      editaAluno(telefoneFixo, telefoneCelular);
    } else {
      salvaAluno(telefoneFixo, telefoneCelular);
    }
  }

  private Telefone criaTelefone(EditText campoTelefoneFixo, TipoTelefone fixo) {
    String numeroFixo = campoTelefoneFixo.getText().toString();
    return new Telefone(numeroFixo, fixo);
  }

  private void salvaAluno(Telefone telefoneFixo, Telefone telefoneCelular) {
    new SalvaAlunoTask(alunoDao, aluno, telefoneFixo, telefoneCelular, telefoneDao, this::finish)
        .execute();
  }

  private void editaAluno(Telefone telefoneFixo, Telefone telefoneCelular) {
    new EditaAlunoTask(alunoDao,
        aluno,
        telefoneFixo,
        telefoneCelular,
        telefoneDao,
        telefonesDoAluno,
        this::finish).execute();
  }


  private void inicializacaoDosCampos() {
    campoNome = findViewById(R.id.ed_nome);
    campoTelefoneFixo = findViewById(R.id.ed_telefone_fixo);
    campoEmail = findViewById(R.id.ed_email);
    campoTelefoneCelular = findViewById(R.id.ed_telefone_celular);
  }

  private void preencheAluno() {
    String nome = campoNome.getText().toString();
    String email = campoEmail.getText().toString();
    aluno.setNome(nome);
    aluno.setEmail(email);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.opcoes_formulario_aluno_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == R.id.salvar_menu) {
      finalizaFormulario();
    }
    return super.onOptionsItemSelected(item);
  }
}
