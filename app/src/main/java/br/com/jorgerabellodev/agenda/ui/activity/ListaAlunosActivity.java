package br.com.jorgerabellodev.agenda.ui.activity;

import static br.com.jorgerabellodev.agenda.ui.activity.ConstantesActivities.CHAVE_ALUNO;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import br.com.jorgerabellodev.agenda.R;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.ui.ListaAlunosView;

public class ListaAlunosActivity extends AppCompatActivity {

  private ListaAlunosView listaAlunosView;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lista_alunos);
    setTitle(getResources().getString(R.string.lista_de_alunos));
    listaAlunosView = new ListaAlunosView(this);
    configuraFabNovoAluno();
    configuraLista();
  }

  @Override
  protected void onResume() {
    super.onResume();
    listaAlunosView.atualizaAlunos();
  }

  private void configuraFabNovoAluno() {
    FloatingActionButton novoAlunoFab = findViewById(R.id.fab_novo_aluno);
    novoAlunoFab.setOnClickListener(v -> abreFormularioNoModoDeCadastro());
  }

  private void abreFormularioNoModoDeCadastro() {
    startActivity(new Intent(this, FormularioAlunoActivity.class));
  }

  private void configuraLista() {
    ListView listaDeAlunos = findViewById(R.id.lv_alunos);
    listaAlunosView.configuraAdapter(listaDeAlunos);
    configuraListenerDeClickPorItem(listaDeAlunos);
    registerForContextMenu(listaDeAlunos);
  }

  private void configuraListenerDeClickPorItem(ListView listaDeAlunos) {
    listaDeAlunos.setOnItemClickListener((adapterView, view, position, id) -> {
      Aluno alunoEscolhido = (Aluno) adapterView.getItemAtPosition(position);
      abreFormularioEmModoDeEdicao(alunoEscolhido);

    });
  }

  private void abreFormularioEmModoDeEdicao(Aluno aluno) {
    Intent vaiParaFormularioActivity = new Intent(ListaAlunosActivity.this,
        FormularioAlunoActivity.class);
    vaiParaFormularioActivity.putExtra(CHAVE_ALUNO, aluno);
    startActivity(vaiParaFormularioActivity);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    getMenuInflater().inflate(R.menu.lista_alunos_menu, menu);
  }

  @Override
  public boolean onContextItemSelected(MenuItem item) {
    int itemId = item.getItemId();
    if (itemId == R.id.remover_aluno) {
      listaAlunosView.confirmaRemocao(item);
    }
    return super.onContextItemSelected(item);
  }

}
