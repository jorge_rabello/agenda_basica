package br.com.jorgerabellodev.agenda.ui.asynctasks;

import br.com.jorgerabellodev.agenda.database.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.model.Telefone;

public class SalvaAlunoTask extends BaseAlunoComTelefoneTask {

  private final AlunoDAO alunoDao;
  private final Aluno aluno;
  private final Telefone telefoneFixo;
  private final Telefone telefoneCelular;
  private final TelefoneDao telefoneDao;

  public SalvaAlunoTask(AlunoDAO alunoDao,
      Aluno aluno,
      Telefone telefoneFixo,
      Telefone telefoneCelular,
      TelefoneDao telefoneDao,
      FinalizadaListener listener) {
    super(listener);
    this.alunoDao = alunoDao;
    this.aluno = aluno;
    this.telefoneFixo = telefoneFixo;
    this.telefoneCelular = telefoneCelular;
    this.telefoneDao = telefoneDao;
  }

  @Override
  protected Void doInBackground(Void... voids) {
    int alunoId = alunoDao.salva(aluno).intValue();
    vinculaAlunoComTelefone(alunoId, telefoneFixo, telefoneCelular);
    telefoneDao.salva(telefoneFixo, telefoneCelular);
    return null;
  }

}
