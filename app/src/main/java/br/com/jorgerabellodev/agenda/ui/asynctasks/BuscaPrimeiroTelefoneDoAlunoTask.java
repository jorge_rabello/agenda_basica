package br.com.jorgerabellodev.agenda.ui.asynctasks;

import android.os.AsyncTask;
import br.com.jorgerabellodev.agenda.database.dao.TelefoneDao;
import br.com.jorgerabellodev.agenda.model.Telefone;

public class BuscaPrimeiroTelefoneDoAlunoTask extends AsyncTask<Void, Void, Telefone> {

  private final TelefoneDao dao;
  private final int alunoId;
  private final PrimeiroTelefoneEncontradoListener listener;

  public BuscaPrimeiroTelefoneDoAlunoTask(
      TelefoneDao dao,
      int alunoId,
      PrimeiroTelefoneEncontradoListener listener) {

    this.dao = dao;
    this.alunoId = alunoId;
    this.listener = listener;

  }

  @Override
  protected Telefone doInBackground(Void... voids) {
    return dao.buscaPrimeiroTelefoneDoAluno(alunoId);
  }

  @Override
  protected void onPostExecute(Telefone telefone) {
    super.onPostExecute(telefone);
    listener.quandoEncontrado(telefone);
  }

  public interface PrimeiroTelefoneEncontradoListener {

    void quandoEncontrado(Telefone telefoneEncontrado);
  }

}
