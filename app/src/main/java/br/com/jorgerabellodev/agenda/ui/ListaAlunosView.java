package br.com.jorgerabellodev.agenda.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.MenuItem;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import br.com.jorgerabellodev.agenda.database.AgendaDatabase;
import br.com.jorgerabellodev.agenda.database.dao.AlunoDAO;
import br.com.jorgerabellodev.agenda.model.Aluno;
import br.com.jorgerabellodev.agenda.ui.adapter.ListaAlunosAdapter;
import br.com.jorgerabellodev.agenda.ui.asynctasks.BuscaAlunosTask;
import br.com.jorgerabellodev.agenda.ui.asynctasks.RemoveAlunoTask;

public class ListaAlunosView {

  private final ListaAlunosAdapter adapter;
  private final AlunoDAO dao;
  private final Context context;

  public ListaAlunosView(Context context) {
    this.context = context;
    this.adapter = new ListaAlunosAdapter(context);

    AgendaDatabase database = AgendaDatabase.getInstance(context);
    dao = database.getRoomAlunoDAO();

  }

  public void confirmaRemocao(final MenuItem item) {
    new AlertDialog
        .Builder(context)
        .setTitle("Removendo aluno")
        .setMessage("Tem certeza que quer remover o aluno ?")
        .setPositiveButton("Sim", (dialog, which) -> removeAluno(item))
        .setNegativeButton("Não", null)
        .show();
  }

  public void atualizaAlunos() {
    new BuscaAlunosTask(adapter, dao).execute();
  }

  private void remove(Aluno aluno) {
    new RemoveAlunoTask(dao, adapter, aluno).execute();
  }

  private void removeAluno(MenuItem item) {
    AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
    Aluno alunoEscolhido = adapter.getItem(menuInfo.position);
    remove(alunoEscolhido);
  }

  public void configuraAdapter(ListView listaDeAlunos) {
    listaDeAlunos.setAdapter(adapter);
  }

}
